<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyAdmin extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($obj)
    {
        $this->objData = $obj;
        $this->first_name = isset($this->objData['first_name']) ? $this->objData['first_name'] : '';
        $this->last_name = isset($this->objData['last_name']) ? $this->objData['last_name'] : '';
        $this->phone = isset($this->objData['phone']) ? $this->objData['phone'] : '';
        $this->email = isset($this->objData['email']) ? $this->objData['email'] : '';
        $this->car = isset($this->objData['car']) ? $this->objData['car'] : '';
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $template = "Client Name = ".$this->first_name.$this->last_name;
        return $this
            ->from("jatin.premierinfotech@gmail.com",'You Have New Client Inquiry')
            ->subject("Client Inquiry Mail")
            ->html($template);
    }
}

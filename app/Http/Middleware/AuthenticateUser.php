<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AuthenticateUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!isset(\auth()->user()->id)) {
            if($request->ajax())
                return response()->json('404');
            else
                return redirect()->route('login');
        }
        return $next($request);
    }
}

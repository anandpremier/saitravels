<?php

namespace App\Http\Controllers\backend;

use App\Models\backend\Cars;
use App\Models\backend\Cms;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;

class CmsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cmsAbout =Cms::where('page_name','about')->get();

        return view('backend.modules.cms.about.index')->with(compact('cmsAbout'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.modules.cms.about.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(isset($request->editid)){
            $request->validate([
                'page_name' => 'required',
                'description'=> 'required',
            ]);
            $obj =  Cms::findorfail($request->editid);
        }else{
            $request->validate([
                'page_name' => 'required',
                'description'=> 'required',
            ]);
            $obj = new Cms();
        }

        $obj->page_name = $request['page_name'];
        $obj->description = $request['description'];

        $obj->save();

        if(isset($request->editid))
            toastr()->info('Data has been Updated successfully!');
        else
            toastr()->success('Data has been saved successfully!');

        return redirect()->route('admin.cms.about');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cars = Cms::where('id',$id)->get();

        return view('backend.modules.cms.about.edit')->with(compact('cars'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cms::where('id',$id)->delete();
        return response()->json(1);
    }


    public function serviceindex()
    {
        $cmsAbout =Cms::where('page_name','service')->get();

        return view('backend.modules.cms.service.index')->with(compact('cmsAbout'));
    }

    public function servicecreate()
    {
        return view('backend.modules.cms.service.add');
    }


    public function servicestore(Request $request)
    {
        if(isset($request->editid)){
            $request->validate([
                'page_name' => 'required',
                'description'=> 'required',
            ]);
            $obj =  Cms::findorfail($request->editid);
        }else{
            $request->validate([
                'page_name' => 'required',
                'description'=> 'required',
            ]);
            $obj = new Cms();
        }

        $obj->page_name = $request['page_name'];
        $obj->description = $request['description'];

        $obj->save();

        if(isset($request->editid))
            toastr()->info('Data has been Updated successfully!');
        else
            toastr()->success('Data has been saved successfully!');

        return redirect()->route('admin.cms.service');
    }

    public function serviceedit($id)
    {
        $cars = Cms::where('id',$id)->get();

        return view('backend.modules.cms.service.edit')->with(compact('cars'));
    }



    public function servicedestroy($id)
    {
        Cms::where('id',$id)->delete();
        return response()->json(1);
    }

    public function contactindex()
    {
        $cmsAbout =Cms::where('page_name','contact')->get();

        return view('backend.modules.cms.contact.index')->with(compact('cmsAbout'));
    }

    public function contactcreate()
    {
        return view('backend.modules.cms.contact.add');
    }


    public function contactstore(Request $request)
    {
        if(isset($request->editid)){
            $request->validate([
                'page_name' => 'required',
                'description'=> 'required',
            ]);
            $obj =  Cms::findorfail($request->editid);
        }else{
            $request->validate([
                'page_name' => 'required',
                'description'=> 'required',
            ]);
            $obj = new Cms();
        }

        $obj->page_name = $request['page_name'];
        $obj->description = $request['description'];

        $obj->save();

        if(isset($request->editid))
            toastr()->info('Data has been Updated successfully!');
        else
            toastr()->success('Data has been saved successfully!');

        return redirect()->route('admin.cms.contact');
    }

    public function contactedit($id)
    {
        $cars = Cms::where('id',$id)->get();

        return view('backend.modules.cms.contact.edit')->with(compact('cars'));
    }

    public function contactstroy($id)
    {
        Cms::where('id',$id)->delete();
        return response()->json(1);
    }
}

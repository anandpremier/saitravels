<?php

namespace App\Http\Controllers\backend;

use App\Models\backend\Cms;
use App\Models\backend\Seo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SeoController extends Controller
{
    public function index()
    {
        $seo =Seo::get();
        return view('backend.modules.seo.index')->with(compact('seo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $getSeo = [
            'home','about','car','services','contact'
        ];


        return view('backend.modules.seo.add')->with(compact('getSeo'));;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(isset($request->editid)){
            $request->validate([
                'page_name' => 'required',
                'title'=> 'required|max:60',
                'keyword'=> 'required|max:255',
                'description_seo'=> 'required|max:160',
            ]);
            $obj =  Seo::findorfail($request->editid);
        }else{
            $request->validate([
                'page_name' => 'required',
                'title'=> 'required|max:60',
                'keyword'=> 'required|max:255',
                'description_seo'=> 'required|max:160',
            ]);
            $obj = new Seo();
        }

        $obj->page_name = $request['page_name'];
        $obj->title = $request['title'];
        $obj->keyword = $request['keyword'];
        $obj->description = $request['description_seo'];

        $obj->save();

        if(isset($request->editid))
            toastr()->info('Data has been Updated successfully!');
        else
            toastr()->success('Data has been saved successfully!');

        return redirect()->route('admin.seo');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $seo = Seo::where('id',$id)->get();
        $seoOption = [
            'home','about','car','services','contact'
        ];

        $getSeoData = Seo::select('page_name')->get()->toArray();
        foreach ($getSeoData as $data){
           $getSeo =  array_unique( array_merge($seoOption, $data));
        }
        return view('backend.modules.seo.edit')->with(compact('seo','getSeo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Seo::where('id',$id)->delete();
        return response()->json(1);
    }

}

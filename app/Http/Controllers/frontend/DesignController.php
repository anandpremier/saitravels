<?php

namespace App\Http\Controllers\frontend;

use App\Mail\NotifyAdmin;
use App\Models\backend\Cars;
use App\Models\backend\Cms;
use App\Models\backend\Seo;
use App\Models\frontend\Clients;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class DesignController extends Controller
{
    public function index(){

        $cars = Cars::where('status', 1)->get();
        $carsCount = Cars::where('status', 1)->get()->count();
        $seo = Seo::where('page_name','home')->first();
        
        $title = $seo->title;
        $description = $seo->description;
        $keyword = $seo->keyword;

        return view('frontend.pages.index')->with(compact('cars','carsCount','title','description','keyword'));
    }

    public function services(){

        $seo = Seo::where('page_name','services')->first();
        $getContent = Cms::where('page_name','service')->get();
        $title = $seo->title;
        $description = $seo->description;
        $keyword = $seo->keyword;
        return view('frontend.pages.services')->with(compact('title','description','keyword','getContent'));
    }
    public function about(){

        $seo = Seo::where('page_name','about')->first();
        $getContent = Cms::where('page_name','about')->first();
        $title = $seo->title;
        $description = $seo->description;
        $keyword = $seo->keyword;
        return view('frontend.pages.about')->with(compact('title','description','keyword','getContent'));
    }
    public function cars(){

        $seo = Seo::where('page_name','car')->first();

        $title = $seo->title; //'Car - Sai Travels';
        $description = $seo->description;
        $keyword = $seo->keyword;
        $cars =  Cars::where('status', 1)->simplePaginate(6);
        $carsCount = Cars::where('status', 1)->get()->count();
        
        return view('frontend.pages.cars')->with(compact('cars','carsCount','title','description','keyword'));
    }
    public function contact($id = null){


        $seo = Seo::where('page_name','contact')->first();
        $getContent = Cms::where('page_name','contact')->first();
        $title = $seo->title; //'Contact - Sai Travels';
        $description = $seo->description;
        $keyword = $seo->keyword;
        if($id != null){
            $cars = Cars::where('id',$id)
            ->where('status', 1)
                ->select([
                    'id',
                    'name',
                    'image'
                ])
                ->get();
            if($cars->isNotEmpty()){
                $withId = true;

                return view('frontend.pages.contact')->with(compact('cars','withId','title','description','keyword','getContent'));
            }else{
                return redirect()->to('/');
            }
        }
        else{
            $cars = Cars::where('status', 1)->get();
            return view('frontend.pages.contact')->with(compact('cars','title','description','keyword','getContent'));
        }
    }
    public function getCars(Request $request){
        $lastCount = $request->val * 6;
        $firstCount = $lastCount - 6;
        $car = Cars::query()->offset($firstCount)->limit(6)->get();
        return response()->json($car);
    }
    public function contactStore(Request $request){
        $request->validate([
            'car'=>'required|integer',
            'first_name'=>'required',
            'last_name'=>'required',
            'email'=>'required|email',
            'phone'=>'required|numeric',
            'from_date'=>'required|date',
            'to_date'=>'required|date',
            'description'=>'required',
        ]);

        $obj = new Clients();
        $obj->car_id = $request['car'];
        $obj->first_name = $request['first_name'];
        $obj->last_name = $request['last_name'];
        $obj->email = $request['email'];
        $obj->phone = $request['phone'];
        $obj->from_date = date('Y/m/d', strtotime($request['from_date']));
        $obj->to_date = date('Y/m/d', strtotime($request['to_date']));
        $obj->description = $request['description'];
        $obj->save();

        Mail::to('jatin.premierinfotech@gmail.com')->send(new NotifyAdmin($obj));

        toastr()->info('Provided Details Has Been Submitted Successfully !! Thank You');
        return redirect()->route('contact');
    }
}

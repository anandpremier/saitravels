<?php

namespace App\Models\backend;

use Illuminate\Database\Eloquent\Model;

class Seo extends Model
{
    protected $table = "seo_module";
    protected $fillable = ['page_name','title','description','keyword'];
    protected $primaryKey="id";
}

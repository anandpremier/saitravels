<?php

namespace App\Models\backend;

use Illuminate\Database\Eloquent\Model;

class Cars extends Model
{
    protected $table = "cars_data";
    protected $fillable = ['name','image','seat','ac','status'];
    protected $primaryKey="id";
}

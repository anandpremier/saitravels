@include('frontend.layouts.header')

<body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">
<div class="site-wrap" id="home-section">
    <div class="site-mobile-menu site-navbar-target">
        <div class="site-mobile-menu-header">
            <div class="site-mobile-menu-close mt-3">
                <span class="icon-close2 js-menu-toggle"></span>
            </div>
        </div>
        <div class="site-mobile-menu-body"></div>
    </div>
    @include('frontend.layouts.headerNavigation')
    <div class="ftco-blocks-cover-1">
        <div class="ftco-cover-1 overlay innerpage" style="background-image: url({{URL::asset('/frontend/images/hero_2.jpg')}})">
            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="col-lg-6 text-center">
                        <h1>Our For Rent Cars</h1>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="site-section bg-light">
        <div class="container">
            <div class="row" id="html">
                <?php $i=0?>
                @foreach($cars as $data)
                    <?php $i++?>
                    <div class="col-lg-4 col-md-6 mb-4" id="{{$i}}">
                        <div class="item-1">
                            <a href="#"><img id="{{$i}}image" src="{{URL::asset('/cars')."/".$data->image}}" alt="Image" class="img-fluid" style="height: 215px;width: 407px;"></a>
                            <div class="item-1-contents">
                                <div class="text-center">
                                    <h3><a href="#" id="{{$i}}name">{{$data->name}}</a></h3>
                                    <div class="rating">
                                        <span class="icon-star text-warning"></span>
                                        <span class="icon-star text-warning"></span>
                                        <span class="icon-star text-warning"></span>
                                        <span class="icon-star text-warning"></span>
                                        <span class="icon-star text-warning"></span>
                                    </div>
                                    <div class="rent-price"><span>₹ 250/</span>day</div>
                                </div>
                                <ul class="specs">
                                    <li>
                                        <span>Doors</span>
                                        <span class="spec">4</span>
                                    </li>
                                    <li>
                                        <span>Seats</span>
                                        <span class="spec" id="{{$i}}seat">{{$data->seat}}</span>
                                    </li>
                                    <li>
                                        <span>AC-Availability</span>
                                        <span class="spec" id="{{$i}}ac">@if($data->ac)Available @else Not-Available @endif</span>
                                    </li>
                                </ul>
                                <div class="d-flex action">
                                    <a href="{{route('contact',$data->id)}}" class="btn btn-primary">Rent Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="col-md-12">
                {!! $cars->links() !!}
                </div>
               
            </div>
        </div>
    </div>
    <div class="container site-section mb-5">
        <div class="row justify-content-center text-center">
            <div class="col-7 text-center mb-5">
                <h2>How it works</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo assumenda, dolorum necessitatibus eius earum voluptates sed!</p>
            </div>
        </div>
        <div class="how-it-works d-flex">
            <div class="step">
                <span class="number"><span>01</span></span>
                <span class="caption">Time &amp; Place</span>
            </div>
            <div class="step">
                <span class="number"><span>02</span></span>
                <span class="caption">Car</span>
            </div>
            <div class="step">
                <span class="number"><span>03</span></span>
                <span class="caption">Details</span>
            </div>
            <div class="step">
                <span class="number"><span>04</span></span>
                <span class="caption">Checkout</span>
            </div>
            <div class="step">
                <span class="number"><span>05</span></span>
                <span class="caption">Done</span>
            </div>
        </div>
    </div>
    @include('frontend.layouts.footerNavigation')
</div>
@include('frontend.layouts.footer')
</body>
</html>

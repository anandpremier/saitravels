@include('frontend.layouts.header')
<body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">
<div class="site-wrap" id="home-section">
    <div class="site-mobile-menu site-navbar-target">
        <div class="site-mobile-menu-header">
            <div class="site-mobile-menu-close mt-3">
                <span class="icon-close2 js-menu-toggle"></span>
            </div>
        </div>
        <div class="site-mobile-menu-body"></div>
    </div>
    @include('frontend.layouts.headerNavigation')
    <div class="ftco-blocks-cover-1">
        <div class="ftco-cover-1 overlay innerpage" style="background-image: url({{URL::asset('/frontend/images/hero_2.jpg')}}">
            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="col-lg-6 text-center">
                        <h1>Contact Us</h1>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="site-section bg-light" id="contact-section">
        <div class="container">
            <div class="row justify-content-center text-center">
                <div class="col-7 text-center mb-5">
                    <h2>Contact Us Or Use This Form To Rent A Car</h2>
                    <p>We Will Try Respond As Soon As Possible To Provide Our Best Service For Your Satisfaction</p>
                </div>
            </div>
            <div class="row">

                <div class="col-lg-8 mb-5" >
                    <form action="{{route('client.store')}}" method="post" id="clientInquiry">
                        @csrf
                        <div class="form-group row">

                            <div class="col-md-12 mb-4 mb-lg-0">
                                @if(isset($withId))

                                    <select class="form-control" name="car">
                                        @foreach($cars as $carList)
                                            <option value="{{$carList->id}}" selected>{{$carList->name}}</option>
                                        @endforeach
                                    </select>
                                @else
                                    <select class="form-control" name="car">
                                        <option selected disabled>--Select Car--</option>
                                        @foreach($cars as $carList)
                                            <option value="{{$carList->id}}">{{$carList->name}}</option>
                                        @endforeach
                                    </select>

                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6 mb-4 mb-lg-0">
                                <input type="text" class="form-control" name="first_name" placeholder="First name">
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="last_name" placeholder="Last name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6 mb-4 mb-lg-0">
                                <input type="text" class="form-control" name="email" placeholder="Email address">
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="phone" placeholder="Phone Number">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6 mb-4 mb-lg-0">
                                <input type="text" id="from_date cf-3" placeholder="Journey Start Date" name="from_date" class="form-control datepicker px-3">
                            </div>
                            <div class="col-md-6">
                                <input type="text" id="to_date cf-3" placeholder="Journey End Date" name="to_date" class="form-control datepicker px-3">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <textarea class="form-control" placeholder="Write your message." name="description" cols="30" rows="10"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6 mr-auto">
                                <input type="submit" class="btn btn-block btn-primary text-white py-3 px-5" value="Send Message">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-4 ml-auto">
                    <div class="bg-white p-3 p-md-5">
                        <div class="col-md-12" style="    text-align: center;">
                            @if(isset($withId))

                                @foreach($cars as $carList)
                                    <img style="width:300px;" src="<?php echo asset("cars/$carList->image")?>">
                                @endforeach
                            @endif
                        </div>
                            <?php $html = html_entity_decode($getContent->description);
                            echo $html; ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('frontend.layouts.footerNavigation')
</div>
@jquery
@toastr_js
@toastr_render
@include('frontend.layouts.footer')
</body>
</html>
<style>
    .p-md-5 {
        padding: 20px 20px !important;
    }
</style>


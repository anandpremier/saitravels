<header class="site-navbar site-navbar-target" role="banner">
    <div class="quickForm">
        <p class="quickFormp1"><b>info@saitravels.com</b></p>
        <p class="quickFormp2">+91 9374599599</p>
    </div>
    <div class="container">
        <div class="row align-items-center position-relative">
            <div class="col-3 col-sm-6 col-xs-6">
                <div class="site-logo">
                    <a href="{{route('index')}}">Sai Travels</a>
                </div>
            </div>
            <div class="col-9 col-sm-6 col-xs-6  text-right">
                <span class="d-inline-block d-lg-none"><a href="#" class="text-white site-menu-toggle js-menu-toggle py-5 text-white"><span class="icon-menu h3 text-white"></span></a></span>
                <nav class="site-navigation text-right ml-auto d-none d-lg-block" role="navigation">
                    <ul class="site-menu main-menu js-clone-nav ml-auto ">
                        <li class="{{ Route::is('index') ? 'active' : '' }}"><a href="{{route('index')}}" class="nav-link">Home</a></li>
                        <li class="{{ Route::is('services') ? 'active' : '' }}"><a href="{{route('services')}}" class="nav-link">Services</a></li>
                        <li class="{{ Route::is('cars') ? 'active' : '' }}"><a href="{{route('cars')}}" class="nav-link">Cars</a></li>
                        <li class="{{ Route::is('about') ? 'active' : '' }}"><a href="{{route('about')}}" class="nav-link">About</a></li>
                        {{--<li class="{{ Route::is('blog') ? 'active' : '' }}"><a href="{{route('blog')}}" class="nav-link">Blog</a></li>--}}
                        <li class="{{ Route::is('contact') ? 'active' : '' }}"><a href="{{route('contact')}}" class="nav-link">Contact</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>

<script src="{{URL::asset('/frontend/js/jquery-3.3.1.min.js')}}"></script>
<script src="{{URL::asset('/frontend/js/popper.min.js')}}"></script>
<script src="{{URL::asset('/frontend/js/bootstrap.min.js')}}"></script>
<script src="{{URL::asset('/frontend/js/owl.carousel.min.js')}}"></script>
<script src="{{URL::asset('/frontend/js/jquery.sticky.js')}}"></script>
<script src="{{URL::asset('/frontend/js/jquery.waypoints.min.js')}}"></script>
<script src="{{URL::asset('/frontend/js/jquery.animateNumber.min.js')}}"></script>
<script src="{{URL::asset('/frontend/js/jquery.fancybox.min.js')}}"></script>
<script src="{{URL::asset('/frontend/js/jquery.easing.1.3.js')}}"></script>
<script src="{{URL::asset('/frontend/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{URL::asset('/frontend/js/aos.js')}}"></script>
<script src="{{URL::asset('/frontend/js/main.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script>
    $(document).ready(function () {
        $("#clientInquiry").validate({
            rules:{
                car:{
                    required:true,
                    integer:true
                },
                first_name:{
                    required:true,
                },
                last_name:{
                    required:true,
                },
                email:{
                    required:true,
                    email:true
                },
                phone:{
                    required:true,
                    minlength:10,
                    maxlength:10,
                    number:true,
                },
                from_date:{
                    required:true,
                    date:true,
                },
                to_date:{
                    required:true,
                    date:true,
                }
            },
            messages:{
                car:{
                    required:"Please Enter Car Name",
                    integer:"Invalid Selection"
                },
                first_name:{
                    required:"Please Enter First Name",
                },
                last_name:{
                    required:"Please Enter Last Name",
                },
                email:{
                    required:"Please Enter Proper Email-Address",
                    email:"Entered Email-Address is Invalid "
                },
                phone:{
                    required:"Please Enter Proper PhoneNumber",
                    minlength:"Please Enter Proper  PhoneNumber",
                    maxlength:"Please Enter Proper  PhoneNumber",
                    number:"Please Enter Number Only",
                },
                from_date:{
                    required:"Please Select Proper From Date To Proceed",
                    date:"Invalid Input Please Enter Date",
                },
                to_date:{
                    required:"Please Select Proper End Date To Proceed",
                    date:"Invalid Input Please Enter Date",
                }
            }
        });
    });
</script>
<script>
    function paginationCustom(input){
        var url = "{{route('get.cars')}}";
        $.ajax({
            dataType: "json",
            type: "POST",
            url: url,
            data: {
                "_token": "{{ csrf_token() }}",
                "val":input
            },
            success: function (data) {
                console.log(data    );
                for(var i=data.length+1;i<=6;i++){
                    $('#'+i).attr('style','display:none;');
                }
                for(var i=1;i<=data.length;i++){
                    $('#'+i).attr('style','display:show;');
                }
                for(var i=0,j=i+1 ;i<=data.length;i++,j++){
                    $('#'+j+"name").html(data[i].name);
                    $('#'+j+"seat").html(data[i].seat);
                    $('#'+j+"image").attr('src',window.location.origin+"/cars/"+data[i].image);
                    if(data[i].ac === 1)
                        $('#'+j+"ac").html("Available");
                    else
                        $('#'+j+"ac").html("Not-Available");
                }

            }
        });
    }
</script>
<script>
    $(window).on('load',function(){
        $('.owl-nav').attr('style','display:none');
    });
</script>
<script>
    $(document).ready(function(){
        $('.your-class').slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows:true
    });
    });
</script>

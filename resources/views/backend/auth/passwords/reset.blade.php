<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminLTE 2 | Log in</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    @include('backend.auth.layout.header')
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <b>Admin</b>Update Password
    </div>
    <div class="login-box-body">
        <p class="login-box-msg">Enter Your Password Here To Reset </p>
        <form id="adminUpdatePasswordForm" method="POST" action="{{ route('password.update') }}">
            @csrf
            <div class="form-group has-feedback">
                <input type="hidden" name="token" value="{{ $token }}">
                <input id="email" type="email" class="input--style-2 form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus readonly>
                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
                @if(session('status'))
                    <span class="valid-feedback">
                        <strong>{{ session('status') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group has-feedback">
                <input id="password" type="password" placeholder="Enter Your New Password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group has-feedback">
                <input id="password-confirm" type="password" placeholder="Confirm Your New Password" class="form-control" name="password_confirmation" required autocomplete="new-password">
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Update Password</button>
                </div>
            </div>
        </form>
        <br>
        <a href="{{ route('login') }}">I Want To Login</a><br>
    </div>
</div>
@include('backend.auth.layout.footer')
@include('backend.auth.layout.validation')
</body>
</html>

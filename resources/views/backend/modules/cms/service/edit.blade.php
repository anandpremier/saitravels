@include('backend.modules.layout.app')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            &nbsp;&nbsp;&nbsp;Service Form Update 
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Service</li>
            <li class="active"><a onclick="location.reload()" style="cursor: pointer">Add Service</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="col-md-10">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Enter Details Of The Service</h3>
                </div>
                <!--================================ /.box-header =================================================-->
                <!--================================ form start ==================================================-->
                <form id="carForm" enctype="multipart/form-data" action="{{route('admin.cms.service.store')}}" method="post">
                    @csrf
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-md-6 unique">
                                <label>Page Name:</label>
                                <input type="hidden" name="editid" value="{{$cars[0]->id}}"></input>
                                <input type="text" id="page_name" class="form-control" name="page_name"
                                       placeholder="Page Name*:"
                                       value="{{old('page_name') ? old('page_name') : ( ($cars[0]->page_name) ? $cars[0]->page_name : '' )}}">
                                @if($errors->has('page_name'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('page_name') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="col-md-12 unique" style="margin-bottom: 20px;">

                                <label>Description:</label>
                                <textarea name="description" id="description" rows="10" cols="80"
                                          value="{{old('description') ? old('description') : ( ($cars[0]->description) ? $cars[0]->description : '' )}}"></textarea>
                                              @if($errors->has('description'))
                                              <span id="invalid-feedback" role="alert">
                                <strong id="error" style="color: red">{{ $errors->first('description') }}</strong>
                                </span>
                                @endif

                            </div>


                        </div>


                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-lg btn-block">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
@include('backend.modules.layout.foot')
@include('backend.modules.cars.script')

@include('backend.modules.layout.app')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            &nbsp;&nbsp;&nbsp;Contact form
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Contact</li>
            <li class="active"><a href="{{route('admin.cms.contact')}}">Add Contact</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="col-md-10">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Enter Details Of The Contact</h3>
                </div>
                <!--================================ /.box-header =================================================-->
                <!--================================ form start ==================================================-->
                <form id="carForm" enctype="multipart/form-data" action="{{route('admin.cms.contact.store')}}" method="post">
                    @csrf
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-md-6 unique">
                                <label>Page Name:</label>
                                <input type="text" id="page_name" class="form-control" name="page_name"
                                       placeholder="Page Name*:"
                                       value="{{old('page_name')}}">
                                @if($errors->has('page_name'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('page_name') }}</strong>
                                    </span>
                                @endif
                            </div>


                            <div class="col-md-12 unique" style="margin-bottom: 20px;">

                                    <label>Description:</label>
                                    <textarea name="description" id="description" rows="10" cols="80"></textarea>
                                @if($errors->has('description'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>

                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-lg btn-block">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
@include('backend.modules.layout.foot')
@include('backend.modules.cars.script')

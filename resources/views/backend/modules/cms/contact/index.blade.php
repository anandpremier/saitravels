@include('backend.modules.layout.app')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            &nbsp;&nbsp;&nbsp;CMS Management  
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>contact</li>
            <li class="active"><a href="{{route('admin.cms.contact')}}">Manage contact</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">List Of All Contact </h3>
                    <a class="btn btn-primary pull-right" href="{{route('admin.cms.contact.add')}}">
                        <i class="glyphicon glyphicon-plus"></i>
                        Add CMS Content
                    </a>
                </div>
                <div class="box-body">
                    <div class="table-responsive data-table-wrapper">
                        <table id="example" class="ui celled table">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Page name</th>
                                <th>description</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($cmsAbout as $k=> $data)
                                <tr>
                                    <td>{{$data->id}}</td>
                                    <td>{{$data->page_name}}</td>
                                    <td><?php echo  $data->description; ?></td>

                                    <td>
                                        @if($data->status )
                                            <p>Active</p>
                                        @else
                                            <p>Inactive</p>
                                        @endif
                                    </td>
                                    <td>
                                        <a style="color: green;font-size: x-large;cursor: pointer;" href="{{route('admin.cms.contact.edit',$data->id)}}">
                                            <i class="fa fa-edit"></i>
                                        </a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a style="color: red;font-size: x-large;cursor: pointer;" onclick="deleteService({{$data->id}})" data-id="{{$data->id}}">
                                            <i class="fa fa-trash"></i>
                                        </a>&nbsp;&nbsp;&nbsp;&nbsp;
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div><!-- /.box-body -->
            </div>
        </div>
    </section>
</div>
@jquery
@toastr_js
@toastr_render
@include('backend.modules.layout.foot')
@include('backend.modules.cars.script')
<script>
    $(document).ready(function() {
        var table = $('#example').DataTable( {
            lengthChange: false,
        } );
    } );
</script>
<script>
    function deleteService(input){
        var url = "{{route('admin.cms.contact.delete', ":id")}}";
        url = url.replace(':id', input);
        $.ajax({
            dataType: "json",
            type: "POST",
            url: url,
            data: {
                "_token": "{{ csrf_token() }}",
            },
            success: function (data) {
                location.reload();
            }
        });
    }

</script>


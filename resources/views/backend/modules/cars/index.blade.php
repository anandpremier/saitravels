@include('backend.modules.layout.app')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            &nbsp;&nbsp;&nbsp;Cars
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Cars</li>
            <li class="active"><a href="{{route('admin.cars.manage')}}">Manage Cars</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="col-lg-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">List Of All Cars </h3>
                    <a class="btn btn-primary pull-right" href="{{route('admin.cars.add')}}">
                        <i class="glyphicon glyphicon-plus"></i>
                        Add New Car
                    </a>
                </div>
                <div class="box-body">
                    <div class="table-responsive data-table-wrapper">
                        <table id="example" class="ui celled table">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Image</th>
                                <th>Seat</th>
                                <th>Air-Contitioner</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($cars as $k=> $data)
                                <tr>
                                    <td>{{$data->id}}</td>
                                    <td>{{$data->name}}</td>
                                    <td style="text-align: center;"> <img style="width: 200px;" src="<?php echo asset("cars/$data->image")?>"></img>

                                    <td>{{$data->seat}}</td>
                                    <td>
                                        @if($data->ac)
                                            <p>Available</p>
                                        @else
                                            <p>Not-Available</p>
                                        @endif
                                    </td>
                                    <td>
                                        @if($data->status)
                                            <p>Active</p>
                                        @else
                                            <p>Inactive</p>
                                        @endif
                                    </td>
                                    <td>
                                        <a style="color: green;font-size: x-large;cursor: pointer;" href="{{route('admin.cars.edit',$data->id)}}">
                                            <i class="fa fa-edit"></i>
                                        </a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a style="color: red;font-size: x-large;cursor: pointer;" onclick="deleteCar({{$data->id}})" data-id="{{$data->id}}">
                                            <i class="fa fa-trash"></i>
                                        </a>&nbsp;&nbsp;&nbsp;&nbsp;
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div><!-- /.box-body -->
            </div>
        </div>
    </section>
</div>
@jquery
@toastr_js
@toastr_render
@include('backend.modules.layout.foot')
@include('backend.modules.cars.script')
<script>
    $(document).ready(function() {
        var table = $('#example').DataTable( {
            lengthChange: false,
        } );
    } );
</script>



@include('backend.modules.layout.app')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            &nbsp;&nbsp;&nbsp;Car Registration Form
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Cars</li>
            <li class="active"><a href="{{route('admin.cars.add')}}">Add Cars</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="col-md-10">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Enter Details Of The Cars</h3>
                </div>
                <!--================================ /.box-header =================================================-->
                <!--================================ form start ==================================================-->
                <form id="carForm" enctype="multipart/form-data" action="{{route('admin.cars.store')}}" method="post">
                    @csrf
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-md-6 unique">
                                <label>Car Name:</label>
                                <input type="text" id="car_name" class="form-control" name="car_name"
                                       placeholder="Car Name*:"
                                       value="{{old('car_name')}}">
                                @if($errors->has('car_name'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('car_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6 unique">
                                <label>No Of Seats:</label>
                                <input type="number" id="shares" class="form-control" placeholder="No. of Seats* :"
                                       name="seats"
                                       value="{{old('seats')}}">
                                @if($errors->has('seats'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('seats') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 unique">
                                <label>Air-Conditioner Avalibility:</label>
                                <select class="form-control" name="ac" id="ac">
                                    <option selected disabled>--Select Avalibility--</option>
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                                @if($errors->has('ac'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('ac') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6 unique">
                                <label>Air-Conditioner Avalibility:</label>
                                <input type="file" id="image" onchange="readURL(this);" class="form-control" name="image">
                            </div>
                            <div class="col-md-6 unique">
                                <label>Status:</label>
                                <select class="form-control" name="status" id="status">
                                    <option selected disabled>--Select Status--</option>
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 unique">
                                <img src="" id="img">
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-lg btn-block">Add Car</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
@include('backend.modules.layout.foot')
@include('backend.modules.cars.script')

@include('backend.modules.layout.app')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Enquiry Management
            <small>Preview</small>
        </h1>

    </section>
    <section class="content">
        <div class="col-md-10">
            <div class="box box-info">

                <!--================================ /.box-header =================================================-->
                <!--================================ form start ==================================================-->
                <div class="box-body">
                <div class="col-md-3">
                    <p><b>car</b></p>
                    <p>{{ $data->name }}</p>
                </div>
                    <div class="col-md-3">
                        <p><b>First name</b></p>
                        <p>{{ $data1->first_name }}</p>
                    </div>
                    <div class="col-md-3">
                        <p><b>Last name</b></p>
                        <p>{{ $data1->last_name }}</p>
                    </div>
                    <div class="col-md-3">
                        <p><b>Email</b></p>
                        <p>{{ $data1->email }}</p>
                    </div>
                    <div class="col-md-3">
                        <p><b>Phone number</b></p>
                        <p>{{ $data1->phone }}</p>
                    </div>
                    <div class="col-md-3">
                        <p><b>Journey start date</b></p>
                        <p>{{ $data1->from_date }}</p>
                    </div>
                    <div class="col-md-3">
                        <p><b>Journey end date</b></p>
                        <p>{{ $data1->to_date }}</p>
                    </div>
                    <div class="col-md-12">
                        <p><b>Description</b></p>
                        <p>{{ $data1->description }}</p>
                    </div>

                </div>
            </div>
        </div>
    </section>
</div>
@include('backend.modules.layout.foot')
@include('backend.modules.cars.script')

@include('backend.modules.layout.app')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            &nbsp;&nbsp;&nbsp;Seo Content Form
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>About</li>
            <li class="active"><a href="{{route('admin.seo')}}">Add</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="col-md-10">
            <div class="box box-info">

                <!--================================ /.box-header =================================================-->
                <!--================================ form start ==================================================-->
                <form id="carForm" enctype="multipart/form-data" action="{{route('admin.seo.store')}}" method="post">
                    @csrf
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-md-6 unique">
                                <label>Page Name:</label>
                                <select class="form-control" name="page_name" id="page_name">
                                    <option selected disabled>--Select Car--</option>
                                    @foreach($getSeo as $getData)
                                        <option value="{{$getData}}">{{$getData}}</option>
                                    @endforeach

                                </select>
                                @if($errors->has('page_name'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('page_name') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="col-md-6 unique">
                                <label>Title:</label>
                                <input type="text" id="title" class="form-control" name="title"
                                       placeholder="title*:"
                                       value="{{old('title')}}">
                                @if($errors->has('title'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="col-md-6 unique">
                                <label>Keyword:</label>
                                <input type="text" id="title" class="form-control" name="keyword"
                                       placeholder="keyword*:"
                                       value="{{old('keyword')}}">
                                @if($errors->has('keyword'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('keyword') }}</strong>
                                    </span>
                                @endif
                                <p style="color:red">Please enter comma(,) separated values</p>
                            </div>


                            <div class="col-md-6 unique" style="margin-bottom: 20px;">

                                    <label>Description:</label>
                                <input type="text" id="title" class="form-control" name="description_seo"
                                       placeholder="Description*:"
                                       value="{{old('description_seo')}}">
                                @if($errors->has('description_seo'))
                                    <span id="invalid-feedback" role="alert">
                                        <strong id="error" style="color: red">{{ $errors->first('description_seo') }}</strong>
                                    </span>
                                @endif

                            </div>

                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-lg btn-block">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
@include('backend.modules.layout.foot')
@include('backend.modules.cars.script')

-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 11, 2019 at 09:13 AM
-- Server version: 5.7.24
-- PHP Version: 7.1.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `saitravels`
--

-- --------------------------------------------------------

--
-- Table structure for table `cars_data`
--

DROP TABLE IF EXISTS `cars_data`;
CREATE TABLE IF NOT EXISTS `cars_data` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seat` int(11) NOT NULL,
  `ac` tinyint(4) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cars_data`
--

INSERT INTO `cars_data` (`id`, `name`, `image`, `seat`, `ac`, `remember_token`, `created_at`, `updated_at`, `status`) VALUES
(8, 'TOYOTA LIVA', '491e9d0efbde91a583742d7816f9016b.jpg', 5, 0, NULL, '2019-10-24 05:31:36', '2019-12-04 05:24:59', '0'),
(12, 'MARUTI SUZUKI ERTIGA', 'b9f29a9316dca57ba6f649f0417e1ec3.jpg', 8, 1, NULL, '2019-10-24 06:13:53', '2019-12-04 05:33:32', '1'),
(13, 'HONDA CITY', '25969a5657c30fe7c86a8a35742bb9f7.jpg', 5, 1, NULL, '2019-11-03 23:54:27', '2019-11-03 23:54:27', '1'),
(14, 'HYUNDAI XCENT', '67b337718eb35b34cb6a4fa5cce5a646.jpg', 5, 1, NULL, '2019-11-03 23:56:29', '2019-11-03 23:56:29', '1'),
(15, 'TOYOTA INNOVA', 'eaec9292a1b7d0f4a6c6e3ad500384b2.jpg', 8, 1, NULL, '2019-11-04 00:01:55', '2019-11-04 00:01:55', '1'),
(16, 'MARUTI SUZUKI SWIFT', '382ffff77ae33fbd7effd19780d05c39.jpg', 5, 1, NULL, '2019-11-04 00:05:39', '2019-11-04 00:05:39', '1'),
(17, 'TOYOTA INDIGO', '3182b4ffb2aae0ff1940167e3efe46de.jpg', 5, 1, NULL, '2019-11-04 00:06:41', '2019-11-04 00:06:41', '1'),
(18, 'CHEVROLET TAVERA', 'd4095698fd25ebcf6dd60e5525eca68b.jpg', 8, 0, NULL, '2019-11-04 00:08:36', '2019-11-04 00:08:36', '1'),
(19, 'TEMPO TRAVELLER', 'f0fe2e9ae43255d3e59b736b1bbb5c8f.jpg', 14, 1, NULL, '2019-11-04 00:09:19', '2019-11-04 00:09:19', '1'),
(20, 'TOYOTA ETIOS', 'c1bceb36d2b18a5ced19abbbfd47d27f.jpg', 5, 1, NULL, '2019-11-04 00:11:13', '2019-12-04 05:24:52', '0'),
(21, 'Lamborghini Aventador', 'd3d69d5fb0014528fc8d66c936691807.jpg', 2, 1, NULL, '2019-11-04 08:30:48', '2019-11-04 08:30:48', '1'),
(22, 'cdfds', '5cebe08f00ed52f3eb4e5d8d77b34644.jpg', 22, 0, NULL, '2019-12-04 05:17:32', '2019-12-04 05:21:35', '0');

-- --------------------------------------------------------

--
-- Table structure for table `cms_page`
--

DROP TABLE IF EXISTS `cms_page`;
CREATE TABLE IF NOT EXISTS `cms_page` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `page_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT '0-inactive,1-active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_page`
--

INSERT INTO `cms_page` (`id`, `page_name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(5, 'service', '<h3>Local Ahmedabad Taxi</h3>\r\n\r\n<p>Spend a day in Ahmedabad, a city whose hospitality has touched the spirits of many. Finding cab services in Ahmedabad is simple. Just book a day&#39;s taxi with Jay Ambe travels and shop and tour to your heart&#39;s content. We offer a package of 8 hours or 80 kilometers a day. You can extend the mentioned limit at a minimal cost. The additional cost will be calculated on each extra hour spent or each extra kilometers travelled. SConvention, sightseeing or shopping, rest assured that with us you will get the finest taxi service of Ahmedabad.</p>', '1', '2019-12-05 06:08:08', '2019-12-11 03:07:39'),
(6, 'contact', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever&nbsp;</p>', '1', '2019-12-05 06:16:34', '2019-12-11 03:41:14'),
(7, 'service', '<h3>Airport Taxi Services</h3>\r\n\r\n<p>Have you arrived at Ahmedabad airport at 1 AM and found it difficult to locate a taxi near you? Don&#39;t worry, and book a cab with Jay Ambe Travels. Providing 24*7 taxi service for airports is our forte. You can hire us for one side transfer or for a to and fro journey. We are one of the top cab services in Ahmedabad and for us your convenience and safety is paramount. For your back to back meetings rely on us. Our well-mannered drivers will make your work day a smooth sailing affair. You can also plan a day trip with us and with visit the city for leisure.</p>', '1', '2019-12-05 06:16:44', '2019-12-11 03:07:51'),
(13, 'service', '<h3>Corporate Taxi Services</h3>\r\n\r\n<p>Do you frequently have outstation colleagues and associates visiting your Ahmedabad office? Does your staff regularly travel to Baroda for meetings? For all your travel needs tie-up with Jay Ambe Travels. Our car rental service for corporates gives you the ease to book a taxi at the last minute at a predefined price. This saves you the hassle of finding a new cab vendor every time. In addition to this, punctuality is a vital part of our system, and that is why we are serving several big business houses in Ahmedabad. Be it a board meeting, a conference or a corporate dinner, with us you will surely reach your destination on time.</p>', '1', '2019-12-11 03:08:03', '2019-12-11 03:08:03'),
(12, 'about', '<h2>Sai Travels</h2>\r\n\r\n<p>We rents automobiles for short periods of time, generally ranging from a few hours to a few weeks. It is often organised with numerous local branches (which allow a user to return a vehicle to a different location), and primarily located near airports or busy city areas and often complemented by a website allowing online reservations..</p>\r\n\r\n<p>We primarily serve people who require a temporary vehicle, for example, those who do not own their own car, travelers who are out of town, or owners of damaged or destroyed vehicles who are awaiting repair or insurance compensation. We may also serve the self-moving industry needs, by renting vans or trucks, and in certain markets, other types of vehicles such as motorcycles or scooters may also be offered.</p>', '1', '2019-12-11 03:05:41', '2019-12-11 03:05:41');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_10_24_064430_cars', 2),
(4, '2019_10_25_103539_create_register_client_table', 3),
(5, '2019_12_04_104311_alter_car_table', 4),
(6, '2019_12_05_070312_create_cms_page_table', 5),
(7, '2019_12_05_071005_create_cmspage_table', 6),
(8, '2019_12_05_081340_alter_cmd_name_cms_page', 7),
(9, '2019_12_05_082133_alter_cmd_name_change_cms_page', 8),
(10, '2019_12_09_065051_create_seo_module_table', 9),
(11, '2019_12_09_070532_alter_seo_module_table', 10);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('jatin.premierinfotech@gmail.com', '$2y$10$9pex0XGeSoMP5oTPwEhUB.wN3PNoItxjLIsqUycsAsw7ge191Q32q', '2019-11-22 06:40:57');

-- --------------------------------------------------------

--
-- Table structure for table `register_client`
--

DROP TABLE IF EXISTS `register_client`;
CREATE TABLE IF NOT EXISTS `register_client` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `car_id` int(11) NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `register_client`
--

INSERT INTO `register_client` (`id`, `car_id`, `first_name`, `last_name`, `email`, `phone`, `description`, `from_date`, `to_date`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 12, 'jatin', 'vyas', 'jatin.premierinfotech@gmail.com', '9924098333', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. ', '2019-10-15', '2019-10-30', NULL, '2019-10-25 06:05:29', '2019-10-25 06:05:29'),
(2, 8, 'jatin', 'vyas', 'jatin.premierinfotech@gmail.com', '9924098333', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. ', '2019-10-15', '2019-10-30', NULL, '2019-10-25 06:06:14', '2019-10-25 06:06:14'),
(3, 12, 'jatin', 'vyas', 'jatin.premierinfotech@gmail.com', '9924098333', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. ', '2019-10-17', '2019-10-16', NULL, '2019-10-25 07:54:12', '2019-10-25 07:54:12'),
(4, 12, 'jatin', 'vyas', 'jatin.premierinfotech@gmail.com', '9924098333', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. ', '2019-10-17', '2019-10-16', NULL, '2019-10-25 07:54:40', '2019-10-25 07:54:40'),
(5, 12, 'dfgdfg', 'gdfsgdf', 'jatin.premierinfotech@gmail.com', '9924098333', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. ', '2019-10-22', '2019-10-09', NULL, '2019-10-25 08:07:43', '2019-10-25 08:07:43'),
(6, 15, 'Testing', 'Testing', 'Testing@gmail.com', '8866366554', NULL, '2019-12-13', '2019-12-31', NULL, '2019-12-04 06:31:48', '2019-12-04 06:31:48'),
(7, 12, 'dfgdg', 'gfdg', 'gdf@gmail.com', '8888888888', 'fdsfdsfdsf', '2019-12-25', '2019-12-25', NULL, '2019-12-04 06:50:14', '2019-12-04 06:50:14');

-- --------------------------------------------------------

--
-- Table structure for table `seo_module`
--

DROP TABLE IF EXISTS `seo_module`;
CREATE TABLE IF NOT EXISTS `seo_module` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `page_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keyword` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `seo_module`
--

INSERT INTO `seo_module` (`id`, `page_name`, `title`, `description`, `keyword`, `created_at`, `updated_at`) VALUES
(1, 'home', 'Home - Sai Travels', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'expedia, travel, cheap flights, hotels', NULL, '2019-12-09 05:20:54'),
(2, 'about', 'About - Sai Travels', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'expedia, travel, cheap flights, hotels', '2019-12-09 03:42:22', '2019-12-09 05:21:40'),
(4, 'services', 'Services - Sai Travels', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'expedia, travel, cheap flights, hotels', '2019-12-09 05:22:24', '2019-12-09 05:22:24'),
(7, 'car', 'Car - Sai Travels', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'expedia, travel, cheap flights, hotels', '2019-12-09 06:12:26', '2019-12-09 06:12:26'),
(8, 'contact', 'Contact- Sai Travels', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'expedia, travel, cheap flights, hotels', '2019-12-09 06:13:15', '2019-12-09 06:13:15');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'jatin', 'info@saitravels.com', NULL, '$2y$10$7tVjro4.Ytxw.5gqmUnri.LtMiGDpsbQyc8dNqSHXKVgPhYp6UnNC', 'QtOWvb1T5IEqiF2rkEWqAtQVisiN2o2znrP5XWJFZw1SbTkrjHtJAcLySCWP', NULL, '2019-10-23 06:36:58');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
